package csui.serbagunabot.bot.model.line;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LineRequestTest {

    private LineRequest lineRequest;

    private static final String SAMPLE_MSG = "Hello World!";
    private static final String SAMPLE_ID = "ABCDE";

    @BeforeEach
    void setUp() {
        lineRequest = new LineRequest();
    }

    @Test
    void testNoConstructorLineRequestShouldHaveNullFields() {
        Assertions.assertNull(lineRequest.getMessage());
        Assertions.assertNull(lineRequest.getUserId());
    }

    @Test
    void testMessageGetterSetterLineRequest() {
        lineRequest.setMessage(SAMPLE_MSG);
        Assertions.assertEquals(SAMPLE_MSG, lineRequest.getMessage());
    }

    @Test
    void testUserIdGetterSetterLineRequest() {
        lineRequest.setUserId(SAMPLE_ID);
        Assertions.assertEquals(SAMPLE_ID, lineRequest.getUserId());
    }

}
