package csui.serbagunabot.bot.model.line;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LineResponseTest {

    private LineResponse lineResponse;

    @BeforeEach
    void setUp() {
        lineResponse = new LineResponse();
    }

    @Test
    void testNoConstructorShouldHaveNullFields() {
        Assertions.assertNull(lineResponse.getResponse());
    }
}
