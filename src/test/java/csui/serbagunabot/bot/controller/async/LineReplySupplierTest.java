package csui.serbagunabot.bot.controller.async;

import static org.mockito.Mockito.when;

import csui.serbagunabot.bot.handler.FeatureHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class LineReplySupplierTest {

    @Mock
    FeatureHandler featureHandler;

    private static final String MSG_SAMPLE = "Hello World!";
    private static final String USER_ID = "XYZ";
    private static final String REPLY_SAMPLE = "Ok";

    private LineReplySupplier lineReplySupplier;

    @BeforeEach
    void setUp() {
        lineReplySupplier = new LineReplySupplier(featureHandler, MSG_SAMPLE, USER_ID);
    }

    @Test
    void testReplySupplierShouldCallHandlerAssigned() {
        when(featureHandler.handleMessage(MSG_SAMPLE, USER_ID)).thenReturn(REPLY_SAMPLE);

        String response = lineReplySupplier.get();
        Assertions.assertEquals(REPLY_SAMPLE, response);
    }

}
