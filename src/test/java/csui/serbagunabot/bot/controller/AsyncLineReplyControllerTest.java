package csui.serbagunabot.bot.controller;

import static org.mockito.Mockito.*;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.response.BotApiResponse;
import csui.serbagunabot.bot.handler.MessageProxyHandler;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AsyncLineReplyControllerTest {

    @Mock
    LineMessagingClient lineMessagingClient;

    @Mock
    MessageProxyHandler messageProxyHandler;

    @InjectMocks
    AsyncLineReplyController asyncLineReplyController;

    @Mock
    CompletableFuture<BotApiResponse> botApiResponseJob;

    private UserSource userSource;
    private TextMessageContent textMessageContent;
    private MessageEvent<TextMessageContent> messageEvent;

    private static final String REPLY_TOKEN_SAMPLE = "ABCDEFG";
    private static final String USER_ID_SAMPLE = "XYZ123";
    private static final String MESSAGE_SAMPLE = "Hello world!";
    private static final String REPLY_SAMPLE = "Reply";

    /**
     * Sets up the Line event handling env, such as user and the message itself.
     */
    @BeforeEach
    void setUp() {
        userSource = UserSource.builder().userId(USER_ID_SAMPLE).build();
        textMessageContent = TextMessageContent.builder().text(MESSAGE_SAMPLE).build();

        messageEvent = MessageEvent.<TextMessageContent>builder()
                .replyToken(REPLY_TOKEN_SAMPLE)
                .source(userSource)
                .message(textMessageContent)
                .timestamp(new Date().toInstant())
                .build();
    }

    @Test
    void testControllerHandleTextEventSuccess() throws Exception {
        lenient().when(messageProxyHandler.handleMessage(anyString(), anyString()))
            .thenReturn(REPLY_SAMPLE);
        lenient().when(botApiResponseJob.get()).thenReturn(null);
        lenient().when(lineMessagingClient.replyMessage(any(ReplyMessage.class)))
                .thenReturn(botApiResponseJob);

        Assertions.assertDoesNotThrow(() -> {
            asyncLineReplyController.handleTextEvent(messageEvent);
        });
    }

    @Test
    void testControllerHandleTextEventAsyncException() throws Exception {
        lenient().when(messageProxyHandler.handleMessage(anyString(), anyString()))
            .thenReturn(REPLY_SAMPLE);
        lenient().when(botApiResponseJob.get()).thenThrow(new InterruptedException());
        lenient().when(lineMessagingClient.replyMessage(any(ReplyMessage.class)))
            .thenReturn(botApiResponseJob);

        Assertions.assertDoesNotThrow(() -> {
            asyncLineReplyController.handleTextEvent(messageEvent);
        });
    }
}
