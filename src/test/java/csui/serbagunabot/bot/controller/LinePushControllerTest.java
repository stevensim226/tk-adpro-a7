package csui.serbagunabot.bot.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.response.BotApiResponse;
import csui.serbagunabot.bot.model.line.LineRequest;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class LinePushControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    LineMessagingClient lineMessagingClient;

    @MockBean
    CompletableFuture<BotApiResponse> botApiResponseJob;

    private static final String USER_ID_SAMPLE = "XYZ123";
    private static final String MESSAGE_SAMPLE = "Hello world!";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testPushMessageShouldSuccess() throws Exception {
        when(lineMessagingClient.pushMessage(any())).thenReturn(botApiResponseJob);
        lenient().when(botApiResponseJob.get()).thenReturn(null);

        mvc.perform(post("/")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mapToJson(new LineRequest(USER_ID_SAMPLE, MESSAGE_SAMPLE))))
            .andExpect(status().isNoContent());

        verify(botApiResponseJob, times(1)).get();
    }

}
