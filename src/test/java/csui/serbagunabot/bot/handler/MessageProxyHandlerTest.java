package csui.serbagunabot.bot.handler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import csui.serbagunabot.bot.model.line.LineResponse;
import feign.FeignException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MessageProxyHandlerTest {

    @Mock
    TransportasiFeign transportasiFeign;

    @Mock
    CoffeeBotFeign coffeeBotFeign;

    @Mock
    BookRSFeign bookRSFeign;

    @InjectMocks
    MessageProxyHandler messageProxyHandler;

    private static final String USER_ID_EXAMPLE = "abcdefg";

    private static final String TRANSPORT_COMMAND = "!transport";
    private static final String TRANSPORT_REPLY_SAMPLE = "Hello Transport";

    private static final String DRIVER_COMMAND = "!driver";
    private static final String DRIVER_REPLY_SAMPLE = "Hello Driver";

    // Feel free to change HELP_MESSAGE and NOT_FOUND_MESSAGE to fit in with the impl
    private static final String HELP_MESSAGE = "Berikut perintah untuk mengakses "
            + "fitur yang tersedia:\n"
            + "!pulsa untuk mengakses fitur pulsa\n"
            + "!transport untuk mengakses fitur transportasi\n"
            + "!coffee untuk mengakses fitur Coffee Shop\n"
            + "!belanja untuk mengakses fitur Belanja Murah\n"
            + "!doctor untuk mengakses fitur BookRS";

    private static final String NOT_FOUND_MESSAGE = "Perintah anda tidak kami kenali, "
            + "ketik !help untuk menunjukkan daftar perintah kembali.";

    private static final String CONN_ERR_MESSAGE = "Fitur yang anda ingin akses saat "
            + "ini tidak bisa diakses, mohon mencoba dalam beberapa saat kembali.";

    @Test
    void testHandleHelpMessageShouldReturnHelp() {
        String message = messageProxyHandler.handleMessage("!help", USER_ID_EXAMPLE);
        Assertions.assertEquals(HELP_MESSAGE, message);
    }

    @Test
    void testHandleNotFoundCommandShouldReturnNotFound() {
        String message = messageProxyHandler.handleMessage("!abcd", USER_ID_EXAMPLE);
        Assertions.assertEquals(NOT_FOUND_MESSAGE, message);
    }

    @Test
    void testHandlePulsaShouldCallPulsaHandler() {
        String message = messageProxyHandler.handleMessage("!pulsa", USER_ID_EXAMPLE);
        // TODO: Add test to verify pulsa handler was called
    }

    @Test
    void testHandleTransportShouldCallTransportHandler() {
        when(transportasiFeign.handleTransportasiMessage(any()))
            .thenReturn(new LineResponse(TRANSPORT_REPLY_SAMPLE));
        String message = messageProxyHandler.handleMessage(TRANSPORT_COMMAND, USER_ID_EXAMPLE);
        Assertions.assertNotNull(message);
    }

    @Test
    void testHandleDriverShouldCallDriverHandler() {
        when(transportasiFeign.handleDriverMessage(any()))
            .thenReturn(new LineResponse(DRIVER_REPLY_SAMPLE));
        String message = messageProxyHandler.handleMessage(DRIVER_COMMAND, USER_ID_EXAMPLE);
        Assertions.assertNotNull(message);
    }

    @Test
    void testHandleCoffeeShouldCallCoffeeHandler() {
        when(coffeeBotFeign.handleCoffeebotMessage(any()))
                .thenReturn(new LineResponse(TRANSPORT_REPLY_SAMPLE));
        String message = messageProxyHandler.handleMessage("!coffee", USER_ID_EXAMPLE);
        Assertions.assertNotNull(message);
    }

    @Test
    void testHandleBelanjaShouldCallBelanjaHandler() {
        String message = messageProxyHandler.handleMessage("!belanja", USER_ID_EXAMPLE);
        // TODO: Add test to verify belanja handler was called
    }

    @Test
    void testHandleDoctorShouldCallDoctorHandler() {
        when(bookRSFeign.handleBookRSmessage(any()))
                .thenReturn(new LineResponse(TRANSPORT_REPLY_SAMPLE));
        String message = messageProxyHandler.handleMessage("!doctor", USER_ID_EXAMPLE);
        Assertions.assertNotNull(message);
    }

    @Test
    void testHandleConnectionErrorHandler() {
        when(transportasiFeign.handleTransportasiMessage(any())).thenThrow(FeignException.class);
        String message = messageProxyHandler.handleMessage("!transport", USER_ID_EXAMPLE);

        Assertions.assertEquals(CONN_ERR_MESSAGE,message);
    }
}
