package csui.serbagunabot.bot.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import csui.serbagunabot.bot.model.line.LineRequest;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles manual bot message sending feature (not tied to replies).
 */
@RestController
public class LinePushController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    Logger logger = LoggerFactory.getLogger("Message Push Logging");

    /**
     * Sends text messages to a user through userId regardles of events.
     */
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<LineRequest> sendTextMessage(@RequestBody LineRequest lineRequest) throws
        ExecutionException, InterruptedException {

        lineMessagingClient
                .pushMessage(new PushMessage(lineRequest.getUserId(),
                    new TextMessage(lineRequest.getMessage())))
                .get();
        logger.info("Success pushing message");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
