package csui.serbagunabot.bot.controller.async;

import csui.serbagunabot.bot.handler.FeatureHandler;
import java.util.function.Supplier;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LineReplySupplier implements Supplier<String> {

    FeatureHandler featureHandler;
    String message;
    String userId;

    @Override
    public String get() {
        return featureHandler.handleMessage(message, userId);
    }
}
