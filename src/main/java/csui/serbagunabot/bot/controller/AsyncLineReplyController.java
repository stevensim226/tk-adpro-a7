package csui.serbagunabot.bot.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import csui.serbagunabot.bot.controller.async.LineReplySupplier;
import csui.serbagunabot.bot.handler.MessageProxyHandler;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Handles incoming user sent messages.
 */
@LineMessageHandler
public class AsyncLineReplyController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private MessageProxyHandler messageProxyHandler;

    Logger logger = LoggerFactory.getLogger("Reply Logging");

    /**
     * Handles text messages sent from user, acts as an entrypoint for reply handling.
     * @param messageEvent event that represents the message sent
     */
    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        String pesan = messageEvent.getMessage().getText();
        String userId = messageEvent.getSource().getSenderId();
        logger.info("Message received: {} from user id = {}", pesan, userId);

        String replyToken = messageEvent.getReplyToken();

        CompletableFuture<String> replyFuture = CompletableFuture
            .supplyAsync(new LineReplySupplier(messageProxyHandler, pesan, userId));

        replyFuture.thenAcceptAsync(reply -> {
            try {
                lineMessagingClient
                    .replyMessage(new ReplyMessage(
                        replyToken,
                        new TextMessage(reply))
                    )
                    .get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error("Error while trying to reply '{}' from user id {}",
                    pesan, userId);
            }

            logger.info("Message '{}' from user id {} has successfully been replied.",
                pesan, userId);
        });

        logger.info("Message '{}' from user id {} is being processed.",
            pesan, userId);
    }
}
