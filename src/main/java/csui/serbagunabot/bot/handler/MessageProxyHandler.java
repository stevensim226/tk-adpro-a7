package csui.serbagunabot.bot.handler;

import csui.serbagunabot.bot.controller.LinePushController;
import csui.serbagunabot.bot.model.line.LineRequest;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Proxy class to connect LineController to other handlers.
 */
@Component
public class MessageProxyHandler implements FeatureHandler {

    @Autowired
    TransportasiFeign transportasiFeign;

    @Autowired
    CoffeeBotFeign coffeeBotFeign;

    @Autowired
    BookRSFeign bookRSFeign;

    @Autowired
    LinePushController linePushController;


    // Help message for !help command
    private static final String HELP_MESSAGE = "Berikut perintah untuk "
            + "mengakses fitur yang tersedia:\n"
            + "!pulsa untuk mengakses fitur pulsa\n"
            + "!transport untuk mengakses fitur transportasi\n"
            + "!coffee untuk mengakses fitur Coffee Shop\n"
            + "!belanja untuk mengakses fitur Belanja Murah\n"
            + "!doctor untuk mengakses fitur BookRS";

    // Message if command input is invalid
    private static final String NOT_FOUND_MESSAGE = "Perintah anda tidak kami kenali, "
            + "ketik !help untuk menunjukkan daftar perintah kembali.";

    private static final String CONN_ERR_MESSAGE = "Fitur yang anda ingin akses saat "
            + "ini tidak bisa diakses, mohon mencoba dalam beberapa saat kembali.";

    /**
     * Acts as a proxy to handle message from LineController to specific handler.
     * @param message Message text received from sender
     * @param userId UserId who sent the message
     * @return List of Line Message instances (can be image, text, etc.)
     */
    public String handleMessage(String message, String userId) {
        LineRequest lineRequest = new LineRequest(message, userId);

        try {
            if (message.startsWith("!help")) {
                return HELP_MESSAGE;
            } else if (message.startsWith("!pulsa")) {
                return "Pulsa access";
            } else if (message.startsWith("!transport")) {
                return transportasiFeign.handleTransportasiMessage(lineRequest).getResponse();
            } else if (message.startsWith("!driver")) {
                return transportasiFeign.handleDriverMessage(lineRequest).getResponse();
            } else if (message.startsWith("!coffee")) {
                return coffeeBotFeign.handleCoffeebotMessage(lineRequest).getResponse();
            } else if (message.startsWith("!belanja")) {
                return "Belanja access";
            } else if (message.startsWith("!doctor")) {
                return bookRSFeign.handleBookRSmessage(lineRequest).getResponse();
            }
            return NOT_FOUND_MESSAGE;
        } catch (FeignException e) {
            return CONN_ERR_MESSAGE;
        }

    }

}
