package csui.serbagunabot.bot.handler;

import csui.serbagunabot.bot.model.line.LineRequest;
import csui.serbagunabot.bot.model.line.LineResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "transportasi", url = "${serbaguna.transportasi-url}")
public interface TransportasiFeign {

    @PostMapping("/")
    LineResponse handleTransportasiMessage(@RequestBody LineRequest lineRequest);

    @PostMapping("/driver")
    LineResponse handleDriverMessage(@RequestBody LineRequest lineRequest);

}
