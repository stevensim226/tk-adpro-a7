package csui.serbagunabot.bot.handler;


import csui.serbagunabot.bot.model.line.LineRequest;
import csui.serbagunabot.bot.model.line.LineResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "bookrs", url = "${serbaguna.bookrs-url}")
public interface BookRSFeign {

    @PostMapping("/")
    LineResponse handleBookRSmessage(@RequestBody LineRequest lineRequest);
}
