package csui.serbagunabot.bot.handler;

public interface FeatureHandler {
    /**
     * Handles message according to logic and then returns a reply message String.
     * @param message message sent from the user
     * @param userId userId of the sender to identification
     * @return reply message's String
     */
    public String handleMessage(String message, String userId);
}
