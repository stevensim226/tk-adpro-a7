package csui.serbagunabot.bot.handler;

import csui.serbagunabot.bot.model.line.LineRequest;
import csui.serbagunabot.bot.model.line.LineResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "coffeebot", url = "${serbaguna.coffeebot-url}")
public interface CoffeeBotFeign {

    @PostMapping("/")
    LineResponse handleCoffeebotMessage(@RequestBody LineRequest lineRequest);

}
