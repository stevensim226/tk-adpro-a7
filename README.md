#### Master Coverage / Pipeline
[![pipeline status](https://gitlab.com/stevensim226/tk-adpro-a7/badges/master/pipeline.svg)](https://gitlab.com/stevensim226/tk-adpro-a7/-/commits/master)
[![coverage report](https://gitlab.com/stevensim226/tk-adpro-a7/badges/master/coverage.svg)](https://gitlab.com/stevensim226/tk-adpro-a7/-/commits/master) 
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?branch=master&project=stevensim226_tk-adpro-a7&metric=alert_status)](https://sonarcloud.io/dashboard?id=stevensim226_tk-adpro-a7&branch=master)

## TK Advanced Programming Kelompok A7
| No |      Nama      |     NPM    |     Fitur     | 
|:--:|:--------------:|:----------:|:-------------:|
|  1 | Fadhil Sultoni | 1606918572 | Pulsa         | 
|  2 | Steven         | 1906293322 | Transportasi  | 
|  3 | Rico Tadjudin  | 1906398364 | CoffeeBot     | 
|  4 | Muhammad Zaki  | 1906400002 | Belanja Murah | 
|  5 | Basyira Sabita | 1906400034 | BookRS        |

## Line Bot Serbaguna Services
LINE ID: `@109tpdsd`

## Development Notes:
To push a code without triggering CI/CD then add `[SKIP CI]` at the start of your commit message.
Only do this if you are pushing non-code edits, **DO NOT USE THIS to skip coverage calculation or commits that contain code changes!!!**

Why do this? Gitlab CI has a limit of 400 minutes per month, and 1 pipeline session can take
~10 minutes on master branch or ~4 minutes on normal branch, so use it wisely!

## SonarCloud 
This repository is scanned for Code Smells, Bugs, etc. on SonarCloud too.
[Link to project's sonarcloud page](https://sonarcloud.io/dashboard?id=stevensim226_tk-adpro-a7)